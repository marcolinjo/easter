import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import EasterEgg from './components/EasterEgg.vue'

createApp(App).component('EasterEgg', EasterEgg).mount('#app')