# EasterEgg

Wir wollen diese Komponente still in unsere Anwendungen einbetten.
Wird sie durch einen talentierten Webentwickler gefunden und aktiviert, kann er hierüber Kontakt zu uns aufnehmen.

Please be kind, its our first one. :)

## Verwendung der Komponente

Nachdem die Komponente mit `npm install bsenergy-easteregg` installiert wurde:

und importiert wurde:
    ```
    import Easter from 'bsenergy-easteregg/src/components/EasterEgg.vue';
    ```

Kann Sie im Template durch `<Easter/>` verwendet werden.

## NPMs mit Vite veröffentlichen

1. Neues öffentliches Repo erstellen
2. Neues Projekt mit Vite anlegen: `vue create .`
3. Komponente in Projekt erstellen:
4. Requirements installieren: `npm install --save--`
5. Rollup Config erstellen:
    ```
    //rollup.config.js
    import resolve from '@rollup/plugin-node-resolve';
    import commonjs from '@rollup/plugin-commonjs';
    import { terser } from 'rollup-plugin-terser';

    export default {
    input: 'rc/index.js',
    output: {
        file: 'dist/my-component.umd.js',
        format: 'umd',
        name: 'MyComponent',
        sourcemap: true,
    },
    plugins: [
        resolve(),
        commonjs(),
        terser(),
    ],
    };
    ```
6. Package.json erstellen:
   ```
   //package.json
   {
    "name": "my-component",
    "version": "1.0.0",
    "description": "My Vue 3 Component",
    "main": "dist/my-component.umd.js",
    "module": "dist/my-component.esm.js",
    "unpkg": "dist/my-component.umd.js",
    "jsdelivr": "dist/my-component.umd.js",
    "scripts": {
        "build": "rollup -c",
        "dev": "rollup -c -w"
    },
    "devDependencies": {
        "@rollup/plugin-commonjs": "^21.0.1",
        "@rollup/plugin-node-resolve": "^13.1.3",
        "rollup": "^2.60.0",
        "rollup-plugin-terser": "^7.0.2"
    }
    }
   ``` 
   7. Builden: `npm run build`
   8. Einloggen: `npm login`
   9. Paket Veröffentlichen: `npm publish`